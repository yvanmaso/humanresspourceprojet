package com.data.entity;

import javax.persistence.*;
import java.util.List;

/**
 * Created by masoy on 21/12/2016.
 */

@Entity
public class Department
{
    private Long id;
    private String location;
    private String name;
    private List<Employee>Employees;

    /**
     * Constructor
     */

    public Department() {


    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @OneToMany(cascade = CascadeType.PERSIST, fetch=FetchType.LAZY)
    public List<Employee> getEmployees() {
        return Employees;
    }

    public void setEmployees(List<Employee> employees) {
        Employees = employees;
    }
}
