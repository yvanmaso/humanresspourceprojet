package com.data.Instance;

import com.data.entity.Carnet;
import com.data.entity.Department;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 * Created by masoy on 21/12/2016.
 */
public class InstanceRh
{
    private EntityManager manager;

    public InstanceRh(EntityManager manager) {
        this.manager = manager;
    }

    public EntityManager getManager() {
        return manager;
    }

    public void setManager(EntityManager manager) {
        this.manager = manager;
    }

    /**
     * M�thode d'initialisation de la connection
     * @return
     */
    private static   EntityManager create_connection()
    {
        EntityManagerFactory factory = Persistence
                .createEntityManagerFactory("HumanRessource");
        EntityManager manager = factory.createEntityManager();
        return manager;



    }

    private void createDepartments(String name,String location)

    {

        Department dep=new Department();
        dep.setLocation(location);
        dep.setName(name);
        this.manager.persist(dep);


    }

    public static void main(String[] args)
    {
        InstanceRh conn=new InstanceRh(create_connection());
        EntityTransaction tx = conn.manager.getTransaction();
        tx.begin();
        for(int i=0;i<=20;i++)
        {
            String nom="RH";
            String location="la Defense";
            conn.createDepartments(nom+i,location+i);
        }
        tx.commit();



    }

}
