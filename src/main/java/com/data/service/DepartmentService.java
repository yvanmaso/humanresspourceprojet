package com.data.service;

import com.data.entity.Department;

import java.util.List;

/**
 * Created by masoy on 21/12/2016.
 */
public interface DepartmentService
{
    Department createDepartment(String location,String name);
    List<Department> getAllDepartments();
    void updateDepartment(Long id,String location,String name);
    void deleteDepartment(Long id);
    Department getDepartmentById(Long id);
}
