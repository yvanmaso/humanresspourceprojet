package com.data.service;

import com.data.entity.Employee;

import java.util.Date;
import java.util.List;

/**
 * Created by masoy on 20/12/2016.
 */
public interface EmployeeService
{

Employee createEmployee(String fname,String position,int salary,Date hireDate,Date birthDate,Long departmerntid);
List<Employee> getAllEmmployees();
void updateEmployee(Long id,String fname,String position,int salary,Date hireDate,Date birthDate,Long departmerntid);
void deleteEmployee(Long id);
Employee getEmployeeById(Long Id);
}
